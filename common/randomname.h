/**
 ** @author:	    浓咖啡
 ** @date:          2018.10.12
 ** @brief:         生成随机姓名，只支持UTF-8编码
 */

#ifndef RANDOMNAME_H
#define RANDOMNAME_H

class RandomName
{
public:
    RandomName();
    virtual ~RandomName();
    const char* getName();

protected:
    //3个中文占9个字符，加结尾符
    char m_szName[10];
};

#endif
