#ifndef TESTPROGRESS_H
#define TESTPROGRESS_H

#include <QWidget>
#include "progressstate.h"
namespace Ui {
class TestProgress;
}

class TestProgress : public QWidget
{
    Q_OBJECT

public:
    explicit TestProgress(QWidget *parent = 0);
    ~TestProgress();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::TestProgress *ui;
    ProgressState *progress;
};

#endif // TESTPROGRESS_H
