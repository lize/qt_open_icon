﻿#include "testprogress.h"
#include "ui_testprogress.h"
#include "gbk.h"

TestProgress::TestProgress(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestProgress)
{
    ui->setupUi(this);
    QVector<QString> list;
    list <<"buy"<<"pay"<<"send out"<<"receive"<<a2w("评价评价评价");
    progress = new ProgressState(list,QString(":/images/resourse/images/progress/progress_finish.png"),QString(":/images/resourse/images/progress/progress_unfinish.png"));
    QVBoxLayout *lay = new QVBoxLayout(ui->widget);
    lay->setMargin(0);
    lay->addWidget(progress);
}

TestProgress::~TestProgress()
{
    delete ui;
}

void TestProgress::on_pushButton_clicked()
{
    if(progress->currentIndex() > 0)
    {
        progress->setCurrentIndex(progress->currentIndex() - 1);
        progress->update();
    }
}

void TestProgress::on_pushButton_2_clicked()
{
    if(progress->currentIndex() < progress->getIndexCount())
    {
        progress->setCurrentIndex(progress->currentIndex() + 1);
        progress->update();
    }
}
