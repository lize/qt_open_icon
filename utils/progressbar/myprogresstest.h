#ifndef MYPROGRESSTEST_H
#define MYPROGRESSTEST_H

#include <QWidget>

namespace Ui {
class MyProgressTest;
}

class MyProgressTest : public QWidget
{
    Q_OBJECT
public:
    explicit MyProgressTest(QWidget *parent = 0);
    ~MyProgressTest();

public Q_SLOTS:
    void updateRender();

private:
    Ui::MyProgressTest *ui;
    int m_value;
    QTimer *m_timer;
};

#endif // MYPROGRESS_H
