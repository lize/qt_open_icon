/**
 ** @author:	 Greedysky
 ** @date:       2019.4.15
 ** @brief:      按钮组件
 */
#ifndef CHECKABLE_H
#define CHECKABLE_H

#include <QAbstractButton>

class QState;
class QStateMachine;
class QSignalTransition;

class Checkable;

class CheckableIcon : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor)
    Q_PROPERTY(qreal iconSize READ iconSize WRITE setIconSize)
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity)
public:
    explicit CheckableIcon(const QIcon &icon, Checkable *parent = 0);

    virtual QSize sizeHint() const override;

    void setIcon(const QIcon &icon);
    inline QIcon icon() const { return m_icon; }

    void setColor(const QColor &color);
    inline QColor color() const { return m_color; }

    void setIconSize(qreal size);
    inline qreal iconSize() const { return m_iconSize; }

    void setOpacity(qreal opacity);
    inline qreal opacity() const { return m_opacity; }

protected:
    virtual void paintEvent(QPaintEvent *event) override;

    Checkable *m_checkable;
    QColor m_color;
    QIcon m_icon;
    qreal m_iconSize, m_opacity;

};


class Checkable : public QAbstractButton
{
    Q_OBJECT
public:
    enum LabelPosition
    {
        LabelPositionLeft,
        LabelPositionRight,
    };

    explicit Checkable(QWidget *parent = nullptr);
    virtual ~Checkable();

    void setLabelPosition(LabelPosition placement);
    LabelPosition labelPosition() const;

    void setCheckedColor(const QColor &color);
    QColor checkedColor() const;

    void setUncheckedColor(const QColor &color);
    QColor uncheckedColor() const;

    void setTextColor(const QColor &color);
    QColor textColor() const;

    void setDisabledColor(const QColor &color);
    QColor disabledColor() const;

    void setCheckedIcon(const QIcon &icon);
    QIcon checkedIcon() const;

    void setUncheckedIcon(const QIcon &icon);
    QIcon uncheckedIcon() const;

    virtual QSize sizeHint() const override;

protected:
    virtual bool event(QEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void paintEvent(QPaintEvent *event) override;

    virtual void setupProperties();

    CheckableIcon *m_checkedIcon, *m_uncheckedIcon;
    QStateMachine *m_stateMachine;
    QState *m_uncheckedState, *m_checkedState;
    QState *m_disabledUncheckedState, *m_disabledCheckedState;
    QSignalTransition *m_uncheckedTransition, *m_checkedTransition;
    Checkable::LabelPosition  m_labelPosition;
    QColor m_checkedColor, m_uncheckedColor, m_textColor, m_disabledColor;

};

#endif
