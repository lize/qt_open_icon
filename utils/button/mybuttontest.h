#ifndef MYBUTTONTEST_H
#define MYBUTTONTEST_H

#include <QWidget>

namespace Ui {
class MyButtonTest;
}

class MyButtonTest : public QWidget
{
    Q_OBJECT

public:
    explicit MyButtonTest(QWidget *parent = 0);
    ~MyButtonTest();

private:
    Ui::MyButtonTest *ui;
};

#endif // MYBUTTONTEST_H
