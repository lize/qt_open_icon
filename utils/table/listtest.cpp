﻿#include "listtest.h"
#include "gbk.h"
ListTest::ListTest(QWidget *parent) :
    QWidget(parent)
{
    this->setWindowTitle(a2w("列表组件"));
    QStringList zonelist;
    zonelist<<(a2w("北京"))<<(a2w("香港"))<<(a2w("首尔"));

    normalList  = new NormalList(zonelist);//普通列表
    navList = new NavList(zonelist, ":/images/resourse/images/list/forward.png");//带箭头的列表

    QVBoxLayout *lay = new QVBoxLayout(this);


    lay->addWidget(normalList);
    lay->addWidget(navList);
}
