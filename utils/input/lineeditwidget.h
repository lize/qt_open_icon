/**
 ** @author:	 Greedysky
 ** @date:       2019.4.19
 ** @brief:      输入框组件
 */
#ifndef LINEEDITWIDGET_H
#define LINEEDITWIDGET_H

#include <QLineEdit>

class QPropertyAnimation;

class LineEditWidget : public QLineEdit
{
    Q_OBJECT
public:
    explicit LineEditWidget(QWidget *parent = nullptr);
    ~LineEditWidget();

    void setColor(const QColor &color);

private Q_SLOTS:
    void valueChanged(const QVariant &value);
    void animationFinished();

protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void focusInEvent(QFocusEvent *event) override;

    bool m_isAnimating;
    float m_currentValue;
    QColor m_color;
    QPropertyAnimation *m_animation;

};

#endif
